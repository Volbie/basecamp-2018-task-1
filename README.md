# BaseCamp 2018 Task 1

C/C++ BaseCamp Task 1, Option 19. Client/Server application to load big (>100mb) files with data protection.

Windows execution requires a compiler with dirent.h support (MinGW for ex.) or downloaded dirent.h at https://web.archive.org/web/20170508053209/http://softagalleria.net:80/download/dirent/?C=M;O=D