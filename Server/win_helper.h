#ifndef WIN_HELPER_H
#define WIN_HELPER_H

#define TRANSFER_BUFFER_SIZE 1024
#define DEFAULT_PORT "8400"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <stdio.h>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <mswsock.h>
#include <iphlpapi.h>

void winSockInit(SOCKET* socket, const char* port);
void winSockListen(SOCKET* serverSocket);
void winSockAccept(SOCKET* serverSocket, SOCKET* clientSocket);
void winSockGetLong(SOCKET* serverSocket, long* num);
void winSockGetData(SOCKET* clientSocket, char* data, long buflen);
void winSockGetFile(SOCKET* serverSocket, SOCKET* clientSocket, FILE* file);
void winSockClose(SOCKET* socket);

#endif // WIN_HELPER_H
