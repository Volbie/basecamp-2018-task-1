#include "helper.h"

int main() {

    printf("Server application started...\n\n");

#ifdef _WIN32

    WSADATA wsaData;

    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
        printf("Failed to initialize WinSock API v2.2, error code: %d\n", WSAGetLastError());

        return 1;
    }
#endif

    pSocket serverSocket = (pSocket)malloc(sizeof(*serverSocket));
    pSocket clientSocket = (pSocket)malloc(sizeof(*clientSocket));


    socketInit(serverSocket, DEFAULT_PORT);
    socketListen(serverSocket);
    socketAccept(serverSocket, clientSocket);

    char fileName[FILENAME_MAX] = { 0 };


    while (1) {

        printf("Waiting for file name from client... ");
        socketGetData(clientSocket, fileName, FILENAME_MAX);
        printf("Success (%s)\n", fileName);

        FILE *filehandle = fopen(fileName, "wb");
        socketGetFile(serverSocket, clientSocket, filehandle);


    }

    //socketClose(clientSocket);
    //printf("\nClient disconnected\n\n");
    //WSACleanup();

    return 0;
}
