#ifndef HELPER_H
#define HELPER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32

    #include "win_helper.h"
    #define pSocket SOCKET*

#endif

#ifdef linux

    #include "uni_helper.h"
    #define pSocket ...

#endif

    int socketInit(pSocket socketArg, const char* port);
    int socketListen(pSocket socketArg);
    int socketAccept(pSocket serverSocket, pSocket clientSocket);
    int socketGetLong(pSocket socketArg, long* pNum);
    int socketGetData(pSocket socketArg, char* buff, long buflen);
    int socketGetFile(pSocket serverSocket, pSocket clientSocket, FILE* file);
    int socketClose(pSocket socketArg);


#endif // HELPER_H
