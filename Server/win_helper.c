#include "win_helper.h"

void winSockInit(SOCKET* socketArg, const char* port) {
// Getting and IPv4, Stream, TCP type adress with IP and port (put in result) --------

    struct addrinfo *result = NULL, hints;

    ZeroMemory(&hints, sizeof (hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    if (getaddrinfo(NULL, port, &hints, &result) != 0) {
        printf("Adress formatting... Error %d\n", WSAGetLastError());

        WSACleanup();

        exit(1);
    }

// Creating a listening socket of this adress type -----------------------------------

    *socketArg = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (*socketArg == INVALID_SOCKET) {
        printf("Socket creation... Error %d\n", WSAGetLastError());

        freeaddrinfo(result);
        WSACleanup();

        exit(1);
    }

// Setup socket to desired adress and port --------------------------------------------

    if (bind(*socketArg, result->ai_addr, (int)result->ai_addrlen) == SOCKET_ERROR) {
        printf("Socket binding... Error %d\n", WSAGetLastError());

        freeaddrinfo(result);
        closesocket(*socketArg);
        WSACleanup();

        exit(1);
    }

    freeaddrinfo(result); // No longer needed

    printf("Created socket on port %s\n", port);
}

void winSockListen(SOCKET* serverSocket)
{
    if (listen(*serverSocket, SOMAXCONN) == SOCKET_ERROR) {
        printf("Listeting error %d\n", WSAGetLastError());
        closesocket(*serverSocket);
        WSACleanup();

        exit(1);
    }

    printf("Server currently listening for connections...\n");

}

void winSockAccept(SOCKET *serverSocket, SOCKET *clientSocket)
{    
    struct sockaddr_in received;
    socklen_t receivedSize = sizeof(received);
    *clientSocket = accept(*serverSocket, (struct sockaddr*)&received, &receivedSize);

    if (*clientSocket == INVALID_SOCKET) {
        printf("Accepting error %d\n", WSAGetLastError());
        closesocket(*serverSocket);
        WSACleanup();

        exit(1);
    }

    printf("\nAccepted connection from %s\n\n", inet_ntoa(received.sin_addr));
}


void winSockGetLong(SOCKET *clientSocket, long *num)
{
    if (recv(*clientSocket, (char*)num, sizeof(num), 0) < 0) {
        printf("Error %d\n", WSAGetLastError());
    }
    else {
        printf("Success (%ld bytes)\n", *num);
    }

}

void winSockGetData(SOCKET* clientSocket, char *data, long buflen)
{
    while (buflen > 0) {
        int num = recv(*clientSocket, data, buflen, 0);

        if (num == SOCKET_ERROR) {
            printf("Error %d ocurred while getting data\n", WSAGetLastError());

            closesocket(*clientSocket);
            WSACleanup();

            exit(1);

        }

        data += num;
        buflen -= num;
    }

}


void winSockGetFile(SOCKET* serverSocket, SOCKET* clientSocket, FILE* file)
{
    long fileSize;

    printf("Getting file size from client... ");
    winSockGetLong(clientSocket, &fileSize);

    long fullSize = fileSize;
    long receivedSize = 0;
    char buffer[TRANSFER_BUFFER_SIZE];

    char currPortStr[5] = { 0 };
    strcpy(currPortStr, DEFAULT_PORT);

    int currPort = atoi(DEFAULT_PORT);

    int changed = 0;
    printf("Receiving file data...\n");
    do {
        int num = ((unsigned int)fileSize > sizeof(buffer) ? (int)sizeof(buffer) : fileSize);

        winSockGetData(clientSocket, buffer, num);

        int offset = 0;

        do {
            size_t written = fwrite(&buffer[offset], 1, (size_t)(num - offset), file);

            if (written < 1) {
                printf("Error ocurred while writing data to file\n");

                closesocket(*clientSocket);
                WSACleanup();

                exit(1);

            }
            offset += written;
        } while (offset < num);

        fileSize -= num;
        receivedSize += num;

        int perc = (int)(((double)receivedSize / fullSize) * 100);

        printf("[%d%%] Received %ld bytes out of %ld bytes", perc, receivedSize, fullSize);


        if ((perc >= 10 && changed == 0) || (perc >= 20 && changed == 1)) {
            ++changed;
            currPort += 5;
            sprintf(currPortStr, "%d", currPort);

            send(*clientSocket, "next", 4, 0);
            winSockClose(clientSocket);
            printf("\nClient disconnected on old port\n");

            winSockInit(serverSocket, currPortStr);
            winSockListen(serverSocket);
            winSockAccept(serverSocket, clientSocket);
            printf("Client connected on new port\n");
        }
        else {
            send(*clientSocket, "stay", 4, 0);
        }

        if (receivedSize != fullSize)
            putchar('\r');
        else
            putchar('\n');
    } while (fileSize > 0);

    printf("Success\n\n");
}

void winSockClose(SOCKET* socket)
{
    if (shutdown(*socket, SD_SEND) == SOCKET_ERROR) {
        printf("Closing socket error %d\n", WSAGetLastError());
        WSACleanup();

        exit(1);
    }

    closesocket(*socket);
}
