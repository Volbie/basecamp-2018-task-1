#include "helper.h"

int socketInit(pSocket socketArg, const char* port) {
#ifdef _WIN32
    winSockInit(socketArg, port);
#endif

#ifdef linux
    ...
#endif

    return 0;
}

int socketListen(pSocket socketArg) {
#ifdef _WIN32
    winSockListen(socketArg);
#endif

#ifdef linux
    ...
#endif

    return 0;
}

int socketAccept(pSocket serverSocket, pSocket clientSocket) {
#ifdef _WIN32
    winSockAccept(serverSocket, clientSocket);
#endif

#ifdef linux
    ...
#endif

    return 0;
}

int socketGetLong(pSocket socketArg, long* pNum) {
#ifdef _WIN32
    winSockGetLong(socketArg, pNum);
#endif

#ifdef linux
    ...
#endif

    return 0;
}

int socketGetData(pSocket socketArg, char* buff, long buflen) {
#ifdef _WIN32
    winSockGetData(socketArg, buff, buflen);
#endif

#ifdef linux
    ...
#endif

    return 0;
}

int socketGetFile(pSocket serverSocket, pSocket clientSocket, FILE* file) {
#ifdef _WIN32
    winSockGetFile(serverSocket, clientSocket, file);
#endif

#ifdef linux
    ...
#endif

    return 0;
}

int socketClose(pSocket socketArg) {
#ifdef _WIN32
    winSockClose(socketArg);
#endif

#ifdef linux
    ...
#endif

    return 0;
}

