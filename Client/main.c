#include "helper.h"

int main() {
    printf("This application let's you transfer a list of files from the client \
to the server.\n\n");

#ifdef _WIN32
    WSADATA wsaData;
    int iResult = 0;

    printf("WSA startup: ");
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("Error %d\n", iResult);

        exit(1);
    }
    else {
        printf("Success\n");
    }
#endif

    pSocket connectSocket = (pSocket)malloc(sizeof(*connectSocket));
    socketInit(connectSocket);

    char serverIP[15] = { 0 };
    printf("Server's IP address: ");
    scanf("%s", serverIP); getchar(); // for the '\n'

    char serverPort[5] = { 0 }; // max IPv4 port is 65535 as it is a 16-bit number
    printf("Server port: ");
    scanf("%s", serverPort); getchar(); // for the '\n'

    socketConnect(connectSocket, serverIP, serverPort);

    char fileName[FILENAME_MAX] = { 0 };

    int fileCount = 0;
    for (fileCount = 1; ; ++fileCount) {
        printf("List of files in client directory:\n");
        printFileList();
        putchar('\n');

        printf("Name of file to send: ");

        scanf ("%[^\n]s", fileName); getchar(); // for '\n'
        putchar('\n');
        /* check file name */

        printf("Reaching selected file: ");

        FILE *hFile = fopen(fileName, "rb");
        if (hFile == NULL) {
            printf("Error %ld\n", GetLastError());
            closesocket(*connectSocket);
            WSACleanup();

            exit(1);
        }
        else {
            printf("Success\n");
        }

        printf("Sending file name to server: ");
        socketSendData(connectSocket, fileName, FILENAME_MAX);
        printf("Success\n");

        socketSendFile(connectSocket, hFile, serverIP, serverPort);

        printf("\nSend more files?(Y/n) ");

        char temp = (char)getchar(); getchar(); // for the '\n'
        if (temp != 'Y' && temp != 'y') {
            break;
        }

        putchar('\n');
    }

    socketClose(connectSocket);

#ifdef _WIN32
    WSACleanup();
#endif

    return 0;
}
