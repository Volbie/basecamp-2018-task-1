#include "helper.h"

void printFileList() {

#ifdef _WIN32
    #include <direct.h>
    #define GetCurrentDir _getcwd
#endif

#ifdef linux
    #include <unistd.h>
    #define GetCurrentDir getcwd
#endif

 char clientPath[FILENAME_MAX];

 if (!GetCurrentDir(clientPath, sizeof(clientPath)))
     {
     exit(1);
     }

    clientPath[sizeof(clientPath) - 1] = '\0';

    DIR *dir;
    struct dirent *ent;

    if ((dir = opendir (clientPath)) != NULL) {

        while ((ent = readdir (dir)) != NULL) {
            printf ("%s\n", ent->d_name);
        }

        closedir (dir);

    }

    else {
    perror ("");

    exit(1);
    }
}

int socketInit(pSocket socketArg)
{
#ifdef _WIN32
    winSockInit(socketArg);
#endif

#ifdef linux
    uniSockInit(...);
#endif

    return 0;
}

int socketConnect(pSocket socketArg, const char* IP, const char* port)
{
#ifdef _WIN32
    winSockConnect(socketArg, IP, port);
#endif

#ifdef linux
    uniSockConnect(...);
#endif

    return 0;
}

int socketSendLong(SOCKET *socketArg, unsigned long num)
{
#ifdef _WIN32
    winSockSendULong(socketArg, num);
#endif

#ifdef linux
    ...
#endif

    return 0;
}

int socketSendData(pSocket socketArg, const char *buff, long buflen)
{
#ifdef _WIN32
    winSockSendData(socketArg, buff, buflen);
#endif

#ifdef linux
    uniSockSendLine(...);
#endif

    return 0;
}

int socketSendFile(pSocket socketArg, FILE* file, const char* IP, const char* port)
{
#ifdef _WIN32
    winSockSendFile(socketArg, file, IP, port);
#endif

#ifdef linux
    ...
#endif

    return 0;
}

int socketMovePort(pSocket socketArg, const char* IP, const char* port)
{
#ifdef _WIN32
    winSockMovePort(socketArg, IP, port);
#endif

#ifdef linux
    ...
#endif

    return 0;
}

int socketClose(pSocket socketArg)
{
#ifdef _WIN32
    winSockClose(socketArg);
#endif

#ifdef linux
    uniSockClose(...);
#endif

    return 0;
}
