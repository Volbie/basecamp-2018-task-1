#include "win_helper.h"

void winSockInit(SOCKET* socketArg) {

    printf("Socket creation: ");
    *socketArg = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (*socketArg == INVALID_SOCKET) {

        printf("Error %d\n", WSAGetLastError());
        WSACleanup();

        exit(1);
    }
    else {
        printf("Success\n");
    }
}

void winSockConnect(SOCKET *socketArg, const char *IP, const char *port)
{
    struct addrinfo *result = NULL, hints;

    ZeroMemory(&hints, sizeof (hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    printf("IP and port formatting: ");
    if (getaddrinfo(IP, port, &hints, &result) != 0) {
        printf("Error %d\n", WSAGetLastError());
        WSACleanup();

        exit(1);
    }
    else {
        printf("Success\n");
    }

    printf("Connecting to server: ");
    if (connect(*socketArg, result->ai_addr, (int)result->ai_addrlen) == SOCKET_ERROR) {
        printf("Error %d\n", WSAGetLastError());
        closesocket(*socketArg);
        *socketArg = INVALID_SOCKET;
        WSACleanup();

        exit(1);
    }
    else {
       printf("Success\n");
    }

    freeaddrinfo(result);
    printf("\nConnected to server %s on port %s\n\n", IP, port);
}

void winSockSendData(SOCKET *socketArg, const char* buff, long buflen)
{
    const char *pbuff = (const char*)buff;
    //printf("Sending data to server:\n");

    while (buflen > 0) {
        int num = send(*socketArg, pbuff, buflen, 0);

        if (num == SOCKET_ERROR) {
            printf("Error %d ocurred while sending data\n", WSAGetLastError());

            closesocket(*socketArg);
            WSACleanup();

            exit(1);

            /*
            if (WSAGetLastError() == WSAEWOULDBLOCK) {  // add select() later

                continue;
            }
            */
        }

        pbuff += num;
        buflen -= num;
    }
}

void winSockSendULong(SOCKET *socketArg, unsigned long num)
{
    if (send(*socketArg, (const char*)&num, sizeof(num), 0) == SOCKET_ERROR) {
        printf("Error %d\n", WSAGetLastError());
        closesocket(*socketArg);
        *socketArg = INVALID_SOCKET;
        WSACleanup();

        exit(1);
    }
    else {
       printf("Success\n");
    }
}

void winSockSendFile(SOCKET* socketArg, FILE* file, const char* IP, const char* port)
{
    printf("Getting file size: ");
    fseek(file, 0, SEEK_END);
    long fileSize = ftell(file);
    long sentSize = 0;
    long fullSize = fileSize;

    char currPortStr[5] = { 0 };
    strcpy(currPortStr, port);

    int currPort = atoi(port);

    rewind(file);

    if (fileSize < 0) {
        printf("Error\n");
        closesocket(*socketArg);
        WSACleanup();

        exit(1);
    }
    else {
        printf("Success (%ld bytes)\n", fileSize);
    }

    printf("Sending file size to server: ");
    winSockSendULong(socketArg, (unsigned long)fileSize);

    printf("Sending file to server...\n");
    char buffer[TRANSFER_BUFFER_SIZE] = { 0 };
    char serverMsg[5] = { 0 };
    do {
        long num = ((unsigned int)fileSize > sizeof(buffer)) ? sizeof(buffer) : fileSize;

        num = (long)fread(buffer, 1, (size_t)num, file);

        if (num < 1) {
            printf("Server respond error\n");
            closesocket(*socketArg);
            WSACleanup();

            exit(1);
        }

        winSockSendData(socketArg, buffer, num);

        recv(*socketArg, serverMsg, 4, 0);
        if (!strcmp(serverMsg, "next")) {
            putchar('\n');
            currPort += 5;
            sprintf(currPortStr, "%d", currPort);
            winSockMovePort(socketArg, IP, currPortStr);
        }

        fileSize -= num;
        sentSize += num;

        printf("[%d%%] Sent %ld bytes out of %ld bytes",
               (int)(((double)sentSize / fullSize) * 100), sentSize, fullSize);

        if (sentSize != fullSize)
            putchar('\r');
        else
            putchar('\n');

    } while (fileSize > 0);

    printf("Success\n");

}

void winSockMovePort(SOCKET *socketArg, const char* IP, const char* port)
{
    winSockClose(socketArg);
    printf("\nDisconnected from old port...\n");

    winSockInit(socketArg);
    winSockConnect(socketArg, IP, port);
    printf("Connected on new port\n");
}

void winSockClose(SOCKET* socketArg)
{
    printf("Closing socket: ");
    if (shutdown(*socketArg, SD_SEND) == SOCKET_ERROR) {
        printf("Error %d\n", WSAGetLastError());

        exit(1);
    }
    else {
        printf("Success\n");
    }

    closesocket(*socketArg);
}
