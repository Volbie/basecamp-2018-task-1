#ifndef WIN_HELPER_H
#define WIN_HELPER_H

#define TRANSFER_BUFFER_SIZE 1024

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <stdio.h>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <mswsock.h>
#include <iphlpapi.h>

void winSockInit(SOCKET* socketArg);
void winSockConnect(SOCKET* socketArg, const char* IP, const char* port);
void winSockSendULong(SOCKET* socketArg, unsigned long num);
void winSockSendData(SOCKET* socketArg, const char* buff, long buflen);
void winSockSendFile(SOCKET* socketArg, FILE* file, const char* IP, const char* port);
void winSockMovePort(SOCKET* socketArg, const char* IP, const char* port);
void winSockClose(SOCKET* socketArg);

#endif // WIN_HELPER_H
