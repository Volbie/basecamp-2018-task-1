#ifndef HELPER_H
#define HELPER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h> // later check if its on unix

#ifdef _WIN32

    #include "win_helper.h"
    #define pSocket SOCKET*

#endif

#ifdef linux

    #include "uni_helper.h"
    #define pSocket ...

#endif

    void printFileList(void);

    int socketInit(pSocket socketArg);
    int socketConnect(pSocket socketArg, const char* IP, const char* port);
    int socketSendLong(pSocket socketArg, unsigned long num);
    int socketSendData(pSocket socketArg, const char* buff, long buflen);
    int socketSendFile(pSocket socketArg, FILE* file, const char* IP, const char* port);
    int socketMovePort(pSocket socketArg, const char* IP, const char* port);
    int socketClose(pSocket socketArg);

#endif // HELPER_H
